﻿using System.IO;
using ProtoBuf;

namespace Contracts
{
    public static class Extensions
    {
        public static byte[] ToProtobufBytes<T>(this T obj)
        {
            using (var ms = new MemoryStream())
            {
                Serializer.Serialize(ms, obj);
                var bytes = ms.ToArray();

                return bytes;
            }
        }

        public static T FromProtobufBytes<T>(this byte[] bytes)
        {
            using (var stream = new MemoryStream(bytes))
            {
                var item = Serializer.Deserialize<T>(stream);
                return item;
            }
        }
    }
}
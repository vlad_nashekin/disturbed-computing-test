﻿using ProtoBuf;

namespace Contracts
{
    [ProtoContract]
    public class Quote
    {
        [ProtoMember(1)]
        public string Symbol { get; set; }


        [ProtoMember(2)]
        public double Last { get; set; }

        [ProtoMember(3)]
        public long PublisherSendDateUtcTicks;
    }
}

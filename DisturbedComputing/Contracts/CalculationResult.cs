﻿using ProtoBuf;

namespace Contracts
{
    [ProtoContract]
    public class CalculationResult
    {
        [ProtoMember(1)]
        public string Symbol { get; set; }

        [ProtoMember(2)]
        public double Value  { get; set; }

        [ProtoMember(3)]
        public string CalculatorName { get; set; }

        [ProtoMember(4)]
        public int ParameterN { get; set; }

        [ProtoMember(5)]
        public bool IsIncompleteValue { get; set; }

        [ProtoMember(6)]
        public long ComputerSendDateUtcTicks;
    }
}
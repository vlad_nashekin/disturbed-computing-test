using System.Collections.Generic;
using Contracts;
using NLog;

namespace DisturbedComputing.Controller
{
    public class SaveResultsController:Controller
    {
        public readonly List<CalculationResult> Results;

        public SaveResultsController(string controllerAddress, ILogger logger = null) : base(controllerAddress, logger)
        {
            Results = new List<CalculationResult>();
        }

        public override void OnResult(string topic, CalculationResult result)
        {
            base.OnResult(topic, result);
            Results.Add(result);
        }
    }
}
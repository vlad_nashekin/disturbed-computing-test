﻿using CommandLine;

namespace DisturbedComputing.Controller
{
    internal class Options
    {
        [Option('c', "ControllerAddress", DefaultValue = "tcp://localhost:6672", Required = false, HelpText = "Controller address")]
        public string ControllerAddress { get; set; }
    }
}
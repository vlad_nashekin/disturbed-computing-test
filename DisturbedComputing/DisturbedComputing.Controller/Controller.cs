﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Contracts;
using NetMQ;
using NetMQ.Sockets;
using NLog;

namespace DisturbedComputing.Controller
{
    public class Controller
    {
        private readonly string _controllerAddress;
        private readonly ILogger _logger;
        private readonly CancellationTokenSource _cts;
        private PullSocket _pullSocket;

        public Controller(string controllerAddress,ILogger logger = null)
        {
            _controllerAddress = controllerAddress;
            _logger = logger??LogManager.GetCurrentClassLogger();
            _cts = new CancellationTokenSource();
            
        }

        public void Start()
        {
            _logger.Info($"Starting Controller on {_controllerAddress}");
            _pullSocket = new PullSocket();
            _pullSocket.Bind(_controllerAddress);

            Task.Factory.StartNew(() =>
            {
                while (!_cts.IsCancellationRequested)
                {
                    var message = _pullSocket.ReceiveMultipartMessage();

                    var topic = message[0].ConvertToString();
                    var result = message[1].Buffer.FromProtobufBytes<CalculationResult>();

                    OnResult(topic, result);
                }
            }, TaskCreationOptions.LongRunning);
        }

        public virtual void OnResult(string topic, CalculationResult result)
        {
            var latency = (DateTime.UtcNow.Ticks - result.ComputerSendDateUtcTicks) / TimeSpan.TicksPerMillisecond;
           _logger.Info($"{topic}.{result.CalculatorName}:{result.Value} IsIncomplete:{result.IsIncompleteValue} Latency:{latency} ms");
        }


        public void Stop()
        {
            _logger.Info("Stopping controller...");
            _cts.Cancel();
            _pullSocket.Dispose();
        }
    }
}

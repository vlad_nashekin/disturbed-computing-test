﻿using System;
using NLog;

namespace DisturbedComputing.Controller
{
    public static class Program
    {
        public static void Main(string[] args)
        {

            var logger = LogManager.GetCurrentClassLogger();

            var options = new Options();
            CommandLine.Parser.Default.ParseArguments(args, options);

            var server = new Controller(options.ControllerAddress);
            try
            {
                server.Start();
                logger.Info("Controller started. Press any key to exit...");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                logger.Error(e, "Error");
            }
            finally
            {
                server.Stop();
            }
        }
    }
}
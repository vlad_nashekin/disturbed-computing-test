﻿using CommandLine;

namespace DisturbedComputing.Publisher
{
    internal class Options
    {
        [Option('p', "PublisherAddress", DefaultValue = "tcp://localhost:6671",Required = false, HelpText = "Publisher address")]
        public string PublisherAddress { get; set; }
    }
}
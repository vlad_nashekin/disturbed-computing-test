using System.Collections.Generic;
using Contracts;

namespace DisturbedComputing.Publisher
{
    public class KnownSeriesPublisherServer : PublisherServer
    {
        private readonly Queue<Quote> _quotes;

        public KnownSeriesPublisherServer(string publisherAddress,IEnumerable<Quote> quotes ) : base(publisherAddress)
        {
            _quotes = new Queue<Quote>(quotes);
        }

        public override Quote GetNextQuote()
        {
            return _quotes.Count != 0 ? _quotes.Dequeue() : null;
        }
    }
}
using System;
using System.Threading.Tasks;
using Contracts;

namespace DisturbedComputing.Publisher
{
    public class Every300MsPublisherServer : PublisherServer
    {
        private readonly Random _random;
        public Every300MsPublisherServer(string publisherAddress) : base(publisherAddress)
        {
            _random = new Random(42);
        }

        public override Quote GetNextQuote()
        {
            Task.Delay(TimeSpan.FromMilliseconds(300)).Wait();
            var symbol = "AAPL";

            var quoteMessage = new Quote
            {
                Symbol = symbol,
                Last = _random.Next(80,150)
            };

            return quoteMessage;
        }
    }
}
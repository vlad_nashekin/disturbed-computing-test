﻿using System;
using NLog;

namespace DisturbedComputing.Publisher
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            
            var logger = LogManager.GetCurrentClassLogger();

            var options = new Options();
            CommandLine.Parser.Default.ParseArguments(args, options);

            var server = new Every300MsPublisherServer(options.PublisherAddress);
            try
            {
                server.Start();
                logger.Info("Server started. Press any key to exit...");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                logger.Error(e, "Error");
            }
            finally
            {
                server.Stop();
            }

        }
    }
}
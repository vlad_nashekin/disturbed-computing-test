﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Contracts;
using NetMQ;
using NetMQ.Sockets;
using NLog;

namespace DisturbedComputing.Publisher
{
    public abstract class PublisherServer
    {
        private readonly string _publisherAddress;
        private readonly ILogger _logger;
        private XPublisherSocket _publisherSocket;
        private readonly CancellationTokenSource _cts;

        protected PublisherServer(string publisherAddress,ILogger logger = null)
        {
            _publisherAddress = publisherAddress;
            _logger = logger??LogManager.GetCurrentClassLogger();
            _cts = new CancellationTokenSource();
        }

        public void Start()
        {
            _logger.Info($"Starting Publisher on {_publisherAddress}");
            _publisherSocket = new XPublisherSocket();

            _publisherSocket.Bind(_publisherAddress);

            //https://github.com/zeromq/netmq/issues/94
            //hack by zeromq design, we should wait till subscribers subscribe
            Task.Delay(TimeSpan.FromMilliseconds(500)).Wait();


            Task.Factory.StartNew(() =>
            {
                while (!_cts.IsCancellationRequested)
                {
                    var quote = GetNextQuote();

                    if (quote != null)
                    {
                        quote.PublisherSendDateUtcTicks = DateTime.UtcNow.Ticks;
                        _publisherSocket.SendMoreFrame(quote.Symbol).SendFrame(quote.ToProtobufBytes());
                        _logger.Info($"Send {quote.Symbol}:{quote.Last}");
                    }
                }
            },TaskCreationOptions.LongRunning);
        }

        public virtual Quote GetNextQuote()
        {
            return null;
        }

        public void Stop()
        {
            _logger.Info($"Stopping Publisher on {_publisherAddress}");
            _cts.Cancel();
            _publisherSocket.Dispose();
        }
    }
}
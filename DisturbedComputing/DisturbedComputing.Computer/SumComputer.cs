using DisturbedComputing.Computer.Calculators;

namespace DisturbedComputing.Computer
{
    public class SumComputer : ComputerBase
    {       
        public SumComputer(string publisherAddress, string controllerAddress, int parameterN)
            : base(publisherAddress, controllerAddress, parameterN,new SumCalculator(parameterN))
        {
        }

        public override string Name => $"Sum_{ParameterN}";
    }
}
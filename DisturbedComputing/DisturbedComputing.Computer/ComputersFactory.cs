﻿namespace DisturbedComputing.Computer
{
    public static class ComputersFactory
    {
        public static ComputerBase Get(string publisherAddress,string controllerAddress,string name, int parameterN)
        {
            switch (name.ToLower())
            {
                case "min":
                    return new MinComputer(publisherAddress,controllerAddress,parameterN);

                case "max":
                    return new MaxComputer(publisherAddress, controllerAddress, parameterN);

                case "sma":
                    return new SmaComputer(publisherAddress, controllerAddress, parameterN);

                case "sd":
                    return new SdComputer(publisherAddress, controllerAddress, parameterN);
                case "sum":
                    return new SumComputer(publisherAddress, controllerAddress, parameterN);

                default:
                    return null;
            }
        }
    }
}
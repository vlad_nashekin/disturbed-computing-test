﻿using CommandLine;

namespace DisturbedComputing.Computer
{
    internal class Options
    {
        [Option('p', "PublisherAddress", DefaultValue = "tcp://localhost:6671", Required = false, HelpText = "Publisher address")]
        public string PublisherAddress { get; set; }

        [Option('c', "ControllerAddress", DefaultValue = "tcp://localhost:6672", Required = false, HelpText = "Controller address")]
        public string ControllerAddress { get; set; }

        [Option('f', "Features", DefaultValue = "sum,max,min,sd,sma", Required = false, HelpText = "Controller address")]
        public string Features { get; set; }

        [Option('n', "Period", DefaultValue = 3, Required = false, HelpText = "Period")]
        public int Period { get; set; }


    }
}
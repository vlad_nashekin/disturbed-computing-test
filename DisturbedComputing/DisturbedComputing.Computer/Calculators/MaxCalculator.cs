using System.Linq;

namespace DisturbedComputing.Computer.Calculators
{
    public class MaxCalculator : WindowCalculator
    {
        public MaxCalculator(int parameterN) : base(parameterN, q=>q.Max())
        {
        }
    }
}
using System.Linq;

namespace DisturbedComputing.Computer.Calculators
{
    public class MinCalculator : WindowCalculator
    {
        public MinCalculator(int parameterN) : base(parameterN, q => q.Min())
        {
        }
    }
}
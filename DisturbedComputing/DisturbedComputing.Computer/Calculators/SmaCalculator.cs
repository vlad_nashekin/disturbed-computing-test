using System.Linq;

namespace DisturbedComputing.Computer.Calculators
{
    public class SmaCalculator : WindowCalculator
    {
        public SmaCalculator(int parameterN) : base(parameterN,
            q => q.Sum()/q.Count)
        {
        }
    }
}
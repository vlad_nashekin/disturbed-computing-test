using System.Collections.Generic;
using Contracts;

namespace DisturbedComputing.Computer.Calculators
{
    public class SumCalculator:ICalculator
    {
        private readonly int _parameterN;
        private int _currentLenght;

        private readonly Queue<double> _currentElements;

        private double _currentSum;

        public SumCalculator(int parameterN)
        {
            _parameterN = parameterN;
            _currentElements = new Queue<double>(parameterN);
        }

        public Result Calculate(Quote quote)
        {
            _currentLenght += 1;
            
            if (_currentLenght < _parameterN)
            {
                _currentElements.Enqueue(quote.Last);
                _currentSum += quote.Last;

                return new Result
                {
                    IsIncomplete = true,
                    Value = _currentSum
                };
            }
            if (_currentLenght == _parameterN)
            {
                _currentElements.Enqueue(quote.Last);
                _currentSum += quote.Last;

                return new Result
                {
                    IsIncomplete = false,
                    Value = _currentSum
                };
            }
            else
            {
                _currentElements.Enqueue(quote.Last);
                _currentSum = _currentSum + quote.Last - _currentElements.Dequeue();
                
                return new Result
                {
                    IsIncomplete = false,
                    Value = _currentSum
                };
            }
        }
    }
}
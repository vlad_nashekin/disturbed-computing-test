using System;
using System.Linq;
using Contracts;

namespace DisturbedComputing.Computer.Calculators
{
    public class SdCalculator : WindowCalculator
    {
        public SdCalculator(int parameterN) : base(parameterN,
            q =>
            {
                var len = q.Count;
                var mean = q.Sum()/len;

                var result = Math.Sqrt((q.Sum(x => Math.Pow(x - mean, 2))/(len-1)));


                return result;
                
            })
        {
        }

        public override double FirstValue(Quote quote)
        {
            return Double.NaN;
        }
    }
}
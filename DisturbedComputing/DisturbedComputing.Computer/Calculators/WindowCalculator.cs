using System;
using System.Collections.Generic;
using Contracts;

namespace DisturbedComputing.Computer.Calculators
{
    public abstract class WindowCalculator : ICalculator
    {
        private readonly int _parameterN;
        private readonly Func<Queue<double>, double> _func;
        private int _currentLenght;

        private readonly Queue<double> _currentElements;

        private double _currentValue;

        protected WindowCalculator(int parameterN, Func<Queue<double>,double> func  )
        {
            _parameterN = parameterN;
            _func = func;
            _currentElements = new Queue<double>(parameterN);
        }

        public virtual double FirstValue(Quote quote)
        {
            return quote.Last;
        }

        public Result Calculate(Quote quote)
        {
            _currentLenght += 1;

            if (_currentLenght == 1)
            {
                _currentElements.Enqueue(quote.Last);
                _currentValue = FirstValue(quote);

                return new Result
                {
                    IsIncomplete = _parameterN != 1,
                    Value = _currentValue
                };

            }
            else if (_currentLenght < _parameterN)
            {
                _currentElements.Enqueue(quote.Last);
                _currentValue = _func(_currentElements);

                return new Result
                {
                    IsIncomplete = true,
                    Value = _currentValue
                };
            }
            if (_currentLenght == _parameterN)
            {
                _currentElements.Enqueue(quote.Last);
                _currentValue = _func(_currentElements);

                return new Result
                {
                    IsIncomplete = false,
                    Value = _currentValue
                };
            }
            else
            {
                _currentElements.Enqueue(quote.Last);
                _currentElements.Dequeue();

                _currentValue = _func(_currentElements);

                return new Result
                {
                    IsIncomplete = false,
                    Value = _currentValue
                };
            }
        }
    }
}
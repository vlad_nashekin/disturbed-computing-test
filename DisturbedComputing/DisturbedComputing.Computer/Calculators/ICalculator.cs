using Contracts;

namespace DisturbedComputing.Computer.Calculators
{
    public interface ICalculator
    {
        Result Calculate(Quote quote);
    }
}
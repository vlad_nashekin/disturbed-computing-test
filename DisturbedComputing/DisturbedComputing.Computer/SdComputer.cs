using DisturbedComputing.Computer.Calculators;

namespace DisturbedComputing.Computer
{
    public class SdComputer : ComputerBase
    {
        public SdComputer(string publisherAddress, string controllerAddress, int parameterN)
            : base(publisherAddress, controllerAddress, parameterN, new SdCalculator(parameterN))
        {
        }

        public override string Name => $"Sd_{ParameterN}";
    }
}
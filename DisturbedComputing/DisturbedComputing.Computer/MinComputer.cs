using DisturbedComputing.Computer.Calculators;

namespace DisturbedComputing.Computer
{
    public class MinComputer : ComputerBase
    {
        public MinComputer(string publisherAddress, string controllerAddress, int parameterN)
            : base(publisherAddress, controllerAddress, parameterN, new MinCalculator(parameterN))
        {
        }

        public override string Name => $"Min_{ParameterN}";
    }
}
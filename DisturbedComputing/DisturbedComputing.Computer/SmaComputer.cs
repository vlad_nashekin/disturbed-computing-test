using DisturbedComputing.Computer.Calculators;

namespace DisturbedComputing.Computer
{
    public class SmaComputer : ComputerBase
    {
        public SmaComputer(string publisherAddress, string controllerAddress, int parameterN)
            : base(publisherAddress, controllerAddress, parameterN, new SmaCalculator(parameterN))
        {
        }

        public override string Name => $"Sma_{ParameterN}";
    }
}
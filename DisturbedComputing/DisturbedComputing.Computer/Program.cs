﻿using System;
using System.Collections.Generic;
using NLog;

namespace DisturbedComputing.Computer
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();

            var options = new Options();
            CommandLine.Parser.Default.ParseArguments(args, options);

            List<ComputerBase> computers = new List<ComputerBase>();

            foreach (var name in options.Features.Split(new []{',',';'},StringSplitOptions.RemoveEmptyEntries))
            {
                var computer = ComputersFactory.Get(options.PublisherAddress, options.ControllerAddress, name, options.Period);
                if (computer != null)
                {
                    computers.Add(computer);
                }
            }

            try
            {
                foreach (var computerBase in computers)
                {
                    computerBase.Start();
                }

                logger.Info("Computers started. Press any key to exit...");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                logger.Error(e, "Error");
            }
            finally
            {
                foreach (var computerBase in computers)
                {
                    computerBase.Stop();
                }
            }
        }
    }
}
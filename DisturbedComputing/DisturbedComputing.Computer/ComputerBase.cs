﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Contracts;
using DisturbedComputing.Computer.Calculators;
using NetMQ;
using NetMQ.Sockets;
using NLog;

namespace DisturbedComputing.Computer
{
    public abstract class ComputerBase
    {
        protected readonly string PublisherAddress;
        private readonly string _controllerAddress;
        private readonly ICalculator _calculator;
        private readonly ILogger _logger;
        private readonly CancellationTokenSource _cts;
        private  SubscriberSocket _subSocket;
        private PushSocket _pushSocket;

        protected ComputerBase(string publisherAddress,string controllerAddress,int parameterN, ICalculator calculator,ILogger logger = null)
        {
            _cts = new CancellationTokenSource();
            PublisherAddress = publisherAddress;
            _controllerAddress = controllerAddress;
            _calculator = calculator;
            _logger = logger??LogManager.GetCurrentClassLogger();
            ParameterN = parameterN;
        }

        public void Start()
        {
            _logger.Info($"Starting SubSocket on {PublisherAddress}");
            _subSocket = new SubscriberSocket();
            _subSocket.Connect(PublisherAddress);
            _subSocket.SubscribeToAnyTopic();


            _logger.Info($"Starting PushSocket on {_controllerAddress}");
            _pushSocket = new PushSocket();
            _pushSocket.Connect(_controllerAddress);

            Task.Factory.StartNew(() =>
            {
                while (!_cts.IsCancellationRequested)
                {
                    var message = _subSocket.ReceiveMultipartMessage();
                    var quote = message[1].Buffer.FromProtobufBytes<Quote>();

                    var latency =(DateTime.UtcNow.Ticks - quote.PublisherSendDateUtcTicks)/TimeSpan.TicksPerMillisecond;
                    _logger.Info($"{quote.Symbol}: {quote.Last}. Latency:{latency} ms");

                    var result = _calculator.Calculate(quote);
                    SendToController(result.Value, result.IsIncomplete, quote);

                }
            }, TaskCreationOptions.LongRunning);
        }

        private void SendToController(double result,bool isDefault, Quote quote)
        {
            var calculationResult = new CalculationResult
            {
                Symbol = quote.Symbol,
                Value = result,
                CalculatorName = Name,
                ParameterN = ParameterN,
                IsIncompleteValue = isDefault,
                ComputerSendDateUtcTicks = DateTime.UtcNow.Ticks
            };

            _pushSocket.SendMoreFrame(quote.Symbol).SendFrame(calculationResult.ToProtobufBytes());
        }

        public abstract string Name { get; }
        public int ParameterN { get; }


        public void Stop()
        {
            _logger.Info("Stopping computer...");
            _cts.Cancel();
            _subSocket.Dispose();
        }
    }
}

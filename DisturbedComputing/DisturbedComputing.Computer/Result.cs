namespace DisturbedComputing.Computer
{
    public class Result
    {
        public double Value { get; set; }

        public bool IsIncomplete { get; set; }
    }
}
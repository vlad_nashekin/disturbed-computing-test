using DisturbedComputing.Computer.Calculators;

namespace DisturbedComputing.Computer
{
    public class MaxComputer : ComputerBase
    {
        public MaxComputer(string publisherAddress, string controllerAddress, int parameterN)
            : base(publisherAddress, controllerAddress, parameterN, new MaxCalculator(parameterN))
        {
        }

        public override string Name => $"Max_{ParameterN}";
    }
}
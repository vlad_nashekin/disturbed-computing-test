﻿
using System;
using System.Linq;
using System.Threading.Tasks;
using DisturbedComputing.Computer;
using DisturbedComputing.Publisher;
using NUnit.Framework;

namespace DisturbedComputing.Tests
{
    [TestFixture]
    public class IntegrationFixture
    {
        [Test]
        public void Should_receive_sum_with_parameter_1()
        {
            var sumComputer = CreateSum(1);
            sumComputer.Start();


            var controller = CreateController();
            controller.Start();

            var publisher = CreatePublisher();
            publisher.Start();

            
            Task.Delay(TimeSpan.FromSeconds(5)).Wait();

            publisher.Stop();
            sumComputer.Stop();
            controller.Stop();

            var results = controller.Results;

            Assert.AreEqual(TestConstants.TestSeries.Select(x=>x.Last).ToArray(),
                results.Select(x=>x.Value).ToArray());

        }

        [Test]
        public void Should_receive_sum_with_parameter_2()
        {
            var sumComputer = CreateSum(2);
            sumComputer.Start();


            var controller = CreateController();
            controller.Start();

            var publisher = CreatePublisher();
            publisher.Start();


            Task.Delay(TimeSpan.FromSeconds(5)).Wait();

            publisher.Stop();
            sumComputer.Stop();
            controller.Stop();

            var results = controller.Results;

            Assert.AreEqual(new double[] { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 },
                results.Select(x => x.Value).ToArray());

        }
        
        [Test]
        public void Should_receive_sum_with_parameters_2_and_1()
        {
            var sumComputer2 = CreateSum(2);
            sumComputer2.Start();

            var sumComputer1 = CreateSum(1);
            sumComputer1.Start();


            var controller = CreateController();
            controller.Start();

            var publisher = CreatePublisher();
            publisher.Start();

            Console.WriteLine($"Logs in {TestContext.CurrentContext.TestDirectory}");
            //Console.WriteLine($"Logs in {TestContext.CurrentContext.WorkDirectory}");

            Task.Delay(TimeSpan.FromSeconds(10)).Wait();

            publisher.Stop();
            sumComputer1.Stop();
            sumComputer2.Stop();
            controller.Stop();

            var results = controller.Results;
            var sum1Array = results.Where(x => x.ParameterN == 1).Select(x=>x.Value).ToArray();
            var sum2Array = results.Where(x => x.ParameterN == 2).Select(x => x.Value).ToArray();


            Assert.AreEqual(TestConstants.TestSeries.Select(x => x.Last).ToArray(),
              sum1Array);

            Assert.AreEqual(new double[] { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19 },
                sum2Array);
        }

        private PublisherServer CreatePublisher()
        {
            var server = new KnownSeriesPublisherServer(TestConstants.PublisherAddress,TestConstants.TestSeries);
            return server;
        }

        private SumComputer CreateSum(int n)
        {
            var sumComputer = new SumComputer(TestConstants.PublisherAddress,TestConstants.ControllerAddress,n);

            return sumComputer;
        }

        private Controller.SaveResultsController CreateController()
        {
            return new Controller.SaveResultsController(TestConstants.ControllerAddress);
        }
    }
}
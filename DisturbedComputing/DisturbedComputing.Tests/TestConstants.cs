﻿using System.Collections.Generic;
using System.Linq;
using Contracts;

namespace DisturbedComputing.Tests
{
    public static class TestConstants
    {
        public static string PublisherAddress = "tcp://localhost:6671";
        public static string ControllerAddress = "tcp://localhost:6672";

        public static IEnumerable<Quote> TestSeries = Enumerable.Range(1, 10).Select(x => new Quote { Symbol = "TestSymbol",Last = x }).ToList();

        public static IEnumerable<Quote> ConstantSeries = Enumerable.Range(1, 10).Select(x => new Quote { Symbol = "TestSymbol", Last = 10 }).ToList();

        public static IEnumerable<Quote> TestSeries2 = new double[]
        {
            3, 2, 1, 4, 3, 1, 5, 5, 5
        }.Select(x => new Quote {Symbol = "TestSymbol", Last = x}).ToList();
    }
}
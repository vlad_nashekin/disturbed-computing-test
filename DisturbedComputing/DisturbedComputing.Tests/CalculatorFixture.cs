﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contracts;
using DisturbedComputing.Computer.Calculators;
using NUnit.Framework;

namespace DisturbedComputing.Tests
{
    [TestFixture]
    public class CalculatorFixture
    {

        //sum
        [Test]
        public void Should_calculate_sum_with_parameter_2()
        {
            var series = Enumerable.Range(1, 10).Select(x => new Quote() {Last = x}).ToList();
            var calculator = new SumCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x=>x.Value).ToArray();

            var expected = new double[] {1, 3, 5, 7, 9, 11, 13, 15, 17,19};

            Assert.AreEqual(expected,actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);

        }

        [Test]
        public void Should_calculate_sum_with_parameter_1()
        {
            var series = TestConstants.TestSeries;
            var calculator = new SumCalculator(1);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.False(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }


        //max
        [Test]
        public void Should_calculate_max_with_parameter_1()
        {
            var series = TestConstants.TestSeries;
            var calculator = new MaxCalculator(1);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.False(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_max_with_parameter_2()
        {
            var series = TestConstants.TestSeries;
            var calculator = new MaxCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_max_with_constant_series()
        {
            var series = TestConstants.ConstantSeries;
            var calculator = new MaxCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).Select(x=>10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_max_with_series2()
        {
            var series = TestConstants.TestSeries2;
            var calculator = new MaxCalculator(3);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new double[] {3, 3, 3, 4, 4, 4, 5, 5, 5};

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.True(result[1].IsIncomplete);
            Assert.False(result[2].IsIncomplete);
        }


        //min
        [Test]
        public void Should_calculate_min_with_parameter_1()
        {
            var series = TestConstants.TestSeries;
            var calculator = new MinCalculator(1);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.False(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_min_with_parameter_2()
        {
            var series = TestConstants.TestSeries;
            var calculator = new MinCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new List<int> {1}.Concat(Enumerable.Range(1, 9)).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_min_with_constant_series()
        {
            var series = TestConstants.ConstantSeries;
            var calculator = new MinCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).Select(x => 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_min_with_series2()
        {
            var series = TestConstants.TestSeries2;
            var calculator = new MinCalculator(3);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new double[] { 3, 2, 1, 1, 1, 1, 1, 1, 5 };

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.True(result[1].IsIncomplete);
            Assert.False(result[2].IsIncomplete);
        }

        //sma

        [Test]
        public void Should_calculate_sma_with_parameter_1()
        {
            var series = TestConstants.TestSeries;
            var calculator = new SmaCalculator(1);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.False(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_sma_with_parameter_2()
        {
            var series = TestConstants.TestSeries;
            var calculator = new SmaCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new List<double> { 1 }.Concat(Enumerable.Range(2,9).Select(x=>x-0.5)).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_sma_with_constant_series()
        {
            var series = TestConstants.ConstantSeries;
            var calculator = new SmaCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = Enumerable.Range(1, 10).Select(x => 10).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_sma_with_series2()
        {
            var series = TestConstants.TestSeries2;
            var calculator = new SmaCalculator(3);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new[] {3,2.5,2,7d/3d,8d/3d,8d/3d,3,11d/3d,5 };

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.True(result[1].IsIncomplete);
            Assert.False(result[2].IsIncomplete);
        }

        //sd

        [Test]
        public void Should_calculate_sd_with_parameter_1()
        {
            var series = TestConstants.TestSeries;
            var calculator = new SdCalculator(1);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new List<double> { Double.NaN, Double.NaN, Double.NaN, Double.NaN,
                Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN , Double.NaN}.ToArray();

           
            Assert.AreEqual(expected, actual);
            Assert.False(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_sd_with_parameter_2()
        {
            var series = TestConstants.TestSeries;
            var calculator = new SdCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new List<double> {Double.NaN, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d, 0.70710678118654757d }.ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_sd_with_constant_series()
        {
            var series = TestConstants.ConstantSeries;
            var calculator = new SdCalculator(2);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new List<double> { Double.NaN }.Concat(Enumerable.Range(1, 9).Select(x=>(double)0)).ToArray();

            Assert.AreEqual(expected, actual);
            Assert.True(result.First().IsIncomplete);
            Assert.False(result[1].IsIncomplete);
        }

        [Test]
        public void Should_calculate_sd_with_series2()
        {
            var series = TestConstants.TestSeries2;
            var calculator = new SdCalculator(3);

            var result = series.Select(x => calculator.Calculate(x)).ToList();
            var actual = result.Select(x => x.Value).ToArray();

            var expected = new[] { Double.NaN, 0.7071068, 1, 1.527525, 1.527525, 1.527525, 2, 2.309401,0 };

            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i],0.000001);
            }
            
            Assert.True(result.First().IsIncomplete);
            Assert.True(result[1].IsIncomplete);
            Assert.False(result[2].IsIncomplete);
        }



    }
}
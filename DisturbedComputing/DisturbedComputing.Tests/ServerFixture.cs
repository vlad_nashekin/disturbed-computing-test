﻿using System;
using System.Threading.Tasks;
using Contracts;
using DisturbedComputing.Publisher;
using NetMQ;
using NetMQ.Sockets;
using NUnit.Framework;

namespace DisturbedComputing.Tests
{
    [TestFixture]
    public class ServerFixture
    {
        [Test]
        public void Should_receive_quotes()
        {
            var server = new Every300MsPublisherServer(TestConstants.PublisherAddress);
            server.Start();
            
            var topic = "AAPL";
            var messageReceived = false;

            using (var subSocket = new SubscriberSocket())
            {
                subSocket.Options.ReceiveHighWatermark = 1000;
                subSocket.Connect(TestConstants.PublisherAddress);
                subSocket.Subscribe(topic);
                Console.WriteLine("Subscriber socket connecting...");


                Task.WhenAny(Task.Factory.StartNew(() =>
                {
                    var message = subSocket.ReceiveMultipartMessage();
                    var receivedTopic = message[0].ConvertToString();

                    Assert.AreEqual(receivedTopic,topic);
                    var quoteMessage = message[1].Buffer.FromProtobufBytes<Quote>();
                    if (quoteMessage.Symbol == topic)
                    {
                        messageReceived = true;
                    }

                }), Task.Delay(TimeSpan.FromSeconds(50))).Wait();
            }
            server.Stop();

            Assert.True(messageReceived);
        }
    }
}

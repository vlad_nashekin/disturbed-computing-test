set CONTROLLER_EXE=%~dp0%DisturbedComputing.Controller\bin\Debug\DisturbedComputing.Controller.exe
set COMPUTER_EXE=%~dp0%DisturbedComputing.Computer\bin\Debug\DisturbedComputing.Computer.exe
set PUBLISHER_EXE=%~dp0%DisturbedComputing.Publisher\bin\Debug\DisturbedComputing.Publisher.exe

start %CONTROLLER_EXE%
start %COMPUTER_EXE%
start %PUBLISHER_EXE%